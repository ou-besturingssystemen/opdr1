#include "shell.h"
#include <gtest/gtest.h>
#include <stdlib.h>
#include <fcntl.h>

// shell to run tests on
#define SHELL "../build/shell -t"
//#define SHELL "/bin/sh"


namespace {

void Execute(std::string command, std::string expectedOutput);
void Execute(std::string command, std::string expectedOutput, std::string expectedOutputFile, std::string expectedOutputFileContent);

TEST(Shell, splitString) {
	std::vector<std::string> expected;

	expected = {};
	EXPECT_EQ(expected, splitString(""));
	EXPECT_EQ(expected, splitString(" "));
	EXPECT_EQ(expected, splitString("  "));

	expected = {"foo"};
	EXPECT_EQ(expected, splitString("foo"));
	EXPECT_EQ(expected, splitString(" foo"));
	EXPECT_EQ(expected, splitString("foo "));
	EXPECT_EQ(expected, splitString(" foo "));
	EXPECT_EQ(expected, splitString("  foo  "));

	expected = {"foo", "bar"};
	EXPECT_EQ(expected, splitString("foo bar"));
	EXPECT_EQ(expected, splitString(" foo  bar"));
	EXPECT_EQ(expected, splitString("  foo   bar  "));

	expected = {"cmd1", "arg1", "<", "inputfile", "|", "cmd2", "arg2", ">", "outputfile"};
	EXPECT_EQ(expected, splitString("cmd1 arg1 < inputfile | cmd2 arg2 > outputfile"));
}

TEST(Shell, ParseCommandLine) {
	EXPECT_EQ((Expression{{Command{{"cat"}}}, "", "", false}), parseCommandLine("cat"));
	EXPECT_EQ((Expression{{Command{{"cat"}}}, "to", "", false}), parseCommandLine("cat > to"));
	EXPECT_EQ((Expression{{Command{{"cat"}}}, "", "from", false}), parseCommandLine("cat < from"));
	EXPECT_EQ((Expression{{Command{{"cat"}}}, "to", "from", false}), parseCommandLine("cat < from > to"));

	EXPECT_EQ((Expression{{Command{{"cat","a1","a2","a3"}}}, "", "", false}), parseCommandLine("cat a1 a2 a3"));
	EXPECT_EQ((Expression{{Command{{"cat","a1","a2","a3"}}}, "to", "", false}), parseCommandLine("cat a1 a2 a3 > to"));
	EXPECT_EQ((Expression{{Command{{"cat","a1","a2","a3"}}}, "", "from", false}), parseCommandLine("cat a1 a2 a3 < from "));
	EXPECT_EQ((Expression{{Command{{"cat","a1","a2","a3"}}}, "to", "from", false}), parseCommandLine("cat a1 a2 a3 < from > to"));

	EXPECT_EQ((Expression{{Command{{"a"}}, Command{{"b"}}}, "", "", false}), parseCommandLine("a | b"));
	EXPECT_EQ((Expression{{Command{{"a"}}, Command{{"b"}}, Command{{"c"}}}, "", "", false}), parseCommandLine("a | b | c"));
	EXPECT_EQ((Expression{{Command{{"a","a1","a2","a3"}}, Command{{"b","b1","b2","b3"}}, Command{{"c","c1","c2","c3"}}}, "", "", false}), parseCommandLine("a a1 a2 a3| b b1 b2 b3 |c c1 c2 c3"));
}

TEST(Shell, ParseCommandLineWrong) {
	// the command lines in these tests should be parsed differently, but we don't implement a whole parser.
	EXPECT_EQ((Expression{{Command{{"cat>out"}}}, "", "", false}), parseCommandLine("cat>out"));
}

TEST(Shell, ReadFromFile) {
	Execute("cat < 1", "line 1\nline 2\nline 3\nline 4");
}

TEST(Shell, ReadFromAndWriteToFile) {
	Execute("cat < 1 > ../foobar", "", "../foobar", "line 1\nline 2\nline 3\nline 4");
}

TEST(Shell, ReadFromAndWriteToFileChained) {
	Execute("cat < 1 | head -n 3 > ../foobar", "", "../foobar", "line 1\nline 2\nline 3\n");
	Execute("cat < 1 | head -n 3 | tail -n 1 > ../foobar", "", "../foobar", "line 3\n");
}

TEST(Shell, WriteToFile) {
	Execute("ls -1 > ../foobar", "", "../foobar", "1\n2\n3\n4\n");
}

TEST(Shell, Execute) {
	//Execute("uname", "Linux\n");
	Execute("ls", "1\n2\n3\n4\n");
	Execute("ls -1", "1\n2\n3\n4\n");
}

TEST(Shell, ExecuteChained) {
	Execute("ls -1 | head -n 2", "1\n2\n");
	Execute("ls -1 | head -n 2 | tail -n 1", "2\n");
}


//////////////// HELPERS

std::string filecontents(const std::string& str) {
	std::string retval;
	int fd = open(str.c_str(), O_RDONLY);
	struct stat st;
	if (fd >= 0 && fstat(fd, &st) == 0) {
		long long size = st.st_size;
		retval.resize(size_t(size));
		char *current = const_cast<char*>(retval.c_str());
		ssize_t left = size;
		while (left > 0) {
			ssize_t bytes = read(fd, current, size_t(left));
			if (bytes == 0 || (bytes < 0 && errno != EINTR))
				break;
			if (bytes > 0) {
				current += bytes;
				left -= bytes;
			}
		}
	}
	if (fd >= 0)
		close(fd);
	return retval;
}

void filewrite(const std::string& str, std::string content) {
	int fd = open(str.c_str(), O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd < 0)
		return;
	while (content.size() > 0) {
		ssize_t written = write(fd, content.c_str(), content.size());
		if (written == -1 && errno != EINTR) {
			std::cout << "error writing file '" << str << "': error " << errno << std::endl;
			break;
		}
		if (written >= 0)
			content = content.substr(size_t(written));
	}
	close(fd);
}

void Execute(std::string command, std::string expectedOutput) {
	char buffer[512];
	std::string dir = getcwd(buffer, sizeof(buffer));
	filewrite("input", command);
	std::string cmdstring = std::string("cd ../test-dir; " SHELL " < '") +  dir + "/input' > '" + dir + "/output' 2> /dev/null";
	system(cmdstring.c_str());
	std::string got = filecontents("output");
	EXPECT_EQ(expectedOutput, got);
}

void Execute(std::string command, std::string expectedOutput, std::string expectedOutputFile, std::string expectedOutputFileContent) {
	char buffer[512];
	std::string dir = getcwd(buffer, sizeof(buffer));
	std::string expectedOutputLocation = "../test-dir/" + expectedOutputFile;
	unlink(expectedOutputLocation.c_str());
	filewrite("input", command);
	std::string cmdstring = std::string("cd ../test-dir; " SHELL " < '") + dir + "/input' > '" + dir + "/output' 2> /dev/null";
	int rc = system(cmdstring.c_str());
	EXPECT_EQ(0, rc);
	std::string got = filecontents("output");
	EXPECT_EQ(expectedOutput, got) << command;
	std::string gotOutputFileContents = filecontents(expectedOutputLocation);
	EXPECT_EQ(expectedOutputFileContent, gotOutputFileContents) << command;
	unlink(expectedOutputLocation.c_str());
}

}
