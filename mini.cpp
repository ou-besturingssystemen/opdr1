/**
	* Shell
	* Operating Systems
	* v18.12.19
	*/

/**
	Hint: F1 (or Control-click) op een functienaam om naar de 'man'-pagina te gaan.
	Hint: F2 (or Control-click) op een functienaam of variabele om naar de definitie te gaan.
	Hint: Ctrl-space voor auto complete functie- en variable-namen.
	*/
#include "shell.h"

// Executeer een Command datastructuur. Als het niet uitgevoerd kan worden, geef foutcode terug. Als het uitgevoerd wordt, wordt het huidige proces vervangen door het nieuwe proces.
int executeCommand(const Command& cmd) {
	auto& parts = cmd.parts;
	return execvp(parts);
}

// raamwerk voor "date | tail -c 10"
int shell(bool showPrompt) {
	// maak hier een communicatiekanaal tussen de twee processen
	pid_t child1 = fork();
	if (child1 == 0) {
		// zorg dat de standaard uitvoer (stdout) omgeleid wordt naar het gedeelde communicatiekanaal
		// ruim niet gebruikte resources op
		Command cmd = {{string("date")}};
		executeCommand(cmd);
		abort(); // als de executable niet gevonden wordt moet het child proces stoppen
	}
	pid_t child2 = fork();
	if (child2 == 0) {
		// zorg dat de standaard invoer (stdint) leest uit het gedeelde communicatiekanaal
		// ruim niet gebruikte resources op
		Command cmd = {{string("tail"), string("-c"), string("5")}};
		executeCommand(cmd);
		abort(); // als de executable niet gevonden wordt moet het child proces stoppen
	}
	// ruim niet gebruikte resources op
	// wacht op child processen
	waitpid(child1, nullptr, 0);
	waitpid(child2, nullptr, 0);
	return 0;
}

int main(int argc, char** argv) {
	bool showPrompt = argc == 1;
	return shell(showPrompt);
}
