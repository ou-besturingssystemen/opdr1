/**
	* Shell
	* Operating Systems
	* v18.12.19
	*/

/**
	Hint: F1 (or Control-click) op een functienaam om naar de 'man'-pagina te gaan.
	Hint: F2 (or Control-click) op een functienaam of variabele om naar de definitie te gaan.
	Hint: Ctrl-space voor auto complete functie- en variable-namen.
	*/
#include "shell.h"

// Executeer een Command datastructuur. Als het niet uitgevoerd kan worden, geef foutcode terug. Als het uitgevoerd wordt, wordt het huidige proces vervangen door het nieuwe proces.
int executeCommand(const Command& cmd) {
	auto& parts = cmd.parts;
	return execvp(parts);
}

int executeExpression(Expression& expression) {
	// check for empty expression
	if (expression.commands.size() == 0)
		return EINVAL;

	// kijk of een intern commando afgehandeld moet worden (zoals 'cd' en 'exit')
	if (expression.commands.size() == 1
		&& expression.commands[0].parts.size() == 2
		&& expression.commands[0].parts[0] == "cd") {
		// handel 'cd' af
		return 0;
	}

	// extern commando's, die uitgevoerd moeten met fork():
	// loop over alle commando's, en knoop de invoer en uitvoeren aan elkaar

	// voor nu een dummy aanroep om een semi werkend programma te hebben
	executeCommand(expression.commands[0]);

	return 0;
}

int shell(bool showPrompt) {
	while (cin.good()) {
		string commandLine = requestCommandLine(showPrompt);
		Expression expression = parseCommandLine(commandLine);
		int rc = executeExpression(expression);
		if (rc != 0)
			cout << strerror(rc) << endl;
	}
	return 0;
}

// NIET VERANDEREN, nodig voor het test raamwerk
int main(int argc, char** argv) {
	bool showPrompt = argc == 1;
	return shell(showPrompt);
}
