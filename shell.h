#pragma once

// function/class definitions you are going to use
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/param.h>
#include <signal.h>
#include <string.h>

#include <vector>

// although it is good habit, you don't have to type '' before many objects by including this line
using namespace std;

struct Command {
	vector<string> parts = {};

	// nodig voor het test raamwerk
	bool operator==(const Command& rhs) const {
		return parts == rhs.parts;
	}
};

struct Expression {
	vector<Command> commands;
	std::string toFile; // if empty no redirection
	std::string fromFile; // if empty no redirection
	bool background = false;

	// nodig voor het test raamwerk
	bool operator==(const Expression& rhs) const {
		return commands == rhs.commands
			&& toFile == rhs.toFile
			&& fromFile == rhs.fromFile
			&& background == rhs.background;
	}
};

string requestCommandLine(bool showPrompt);
void displayPrompt();
int execvp(const vector<string>& args);
vector<string> splitString(const string& str, char delimiter = ' ');
Expression parseCommandLine(string commandLine);
int shell(bool prompt);

// other declarations of methods you want to test (should match exactly)
